
//3. Create a trainer object using object literals.

/*4. Initialize/add the following trainer object properties:
- Name (String)
- Age (Number)
- Pokemon (Array)
- Friends (Object with Array values for properties)*/



let trainer = {
	name: "Cardo",
	age: 30,
	pokemon: ['Zubat', 'Togepi', 'Mewtwo']
	friends: {
		iloilo: ['Joe', 'Jane'
		cebu: ['Eggbert','Byran']
	},

	talk: function() {
		console.log("Go Zubat"
	},
}

console.log(trainer);
trainer.talk()
 



//5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!


function Pokemon(name,level) {
this.name = name
this.level = level
this.health = 2 * level
this.attack = 2 * level


//6. Access the trainer object properties using dot and square bracket notation.

this.tackle = function(target) {
	console.log(`${this.name} tackled ${target.name}`)
    target.health -= this.attack

    console.log(`${target.name}'s health is now ${target.health}' `)
      
      if(target.health <=0) {
      	target.faint()
      }
    

      this.faint = function(){
   	  console.log(this.name + 'fainted.')
   }

}



/*8. Create a constructor for creating a pokemon with the following properties:
- Name (Provided as an argument to the contructor)
- Level (Provided as an argument to the contructor)
- Health (Create an equation that uses the level property)
- Attack (Create an equation that uses the level property)
*/

let bulbasaur = new Pokemon('Pikachu', 20)
console.log(pikachu)


let charmander = new Pokemon('Pikachu', 0)
console.log(charmander)


let squirtle = new Pokemon('Pikachu', 50)
console.log(squirtle)

bulbasaur.tackle(charmander)
squirtle.tackle(bulbasaur)
charmander.tackle(squirtle)


/*








7. Invoke/call the trainer talk object method.

9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
11. Create a faint method that will print out a message of targetPokemon has fainted.
12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
13. Invoke the tackle method of one pokemon object to see if it works as intended.
14. Create a git repository named S18.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code. "s18 Activity"
16. Add the link in Boodle "Javascript Objects"


